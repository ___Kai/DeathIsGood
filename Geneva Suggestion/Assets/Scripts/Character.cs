using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public CharacterName charName;
    public Text nickText;
    public void AssignName(CharacterName characterName)
    {
        charName = characterName;
        if (nickText != null)
        {
            nickText.text = charName.serviceBranch;
        }
    }
}
