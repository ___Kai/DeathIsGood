using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTheBastard : MonoBehaviour
{
    private GameObject splatterEffect;
    public static ShootTheBastard instance;
    public GameObject standardTurretPrefab;
    public GameObject anotherTurretPrefab;
    private void Awake()
    {
        instance = this;
    }
    public GameObject SplatterHead()
    {
        return splatterEffect;
    }
    public void SplatterHead(GameObject turret)
    {
        splatterEffect = turret;
    }
}
