using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpyDetection : MonoBehaviour
{
    public bool isThisTheSpy;
    public Vector3 positionOffSet;
    private GameObject turret;
    ShootTheBastard shootTheBastard;
    private void Start()
    {
        shootTheBastard = ShootTheBastard.instance;
    }
    private void OnMouseDown()
    {
        if (shootTheBastard.SplatterHead() == null)
            return;
        if (turret != null)
        {
            Debug.Log("Stop, stop- he's already dead!");
            return;
        }
        GameObject splatterEffect = shootTheBastard.SplatterHead();
        turret = Instantiate(splatterEffect, transform.position + positionOffSet, transform.rotation);
        WasThatTheSpy();
    }
    void WasThatTheSpy()
    {
        if(!isThisTheSpy)
        {
            Debug.Log("Whoops, guess we're war criminals, now...");
        }
        else if (isThisTheSpy)
        {
            Debug.Log("You killed a spy! Well done, you!");
        }
    }
    private void OnMouseEnter()
    {
        if (shootTheBastard.SplatterHead() == null)
            return;
    }
}
